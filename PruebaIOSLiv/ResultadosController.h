//
//  ResultadosController.h
//  PruebaIOSLiv
//
//  Created by lalo on 27/01/18.
//  Copyright © 2018 devchilango. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductoCell.h"

@interface ResultadosController : UIViewController < UITableViewDelegate, UITableViewDataSource >
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *btnCerrar;

@property (strong, nonatomic) NSArray * resultados;


@end
