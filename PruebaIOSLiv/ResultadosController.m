//
//  ResultadosController.m
//  PruebaIOSLiv
//
//  Created by lalo on 27/01/18.
//  Copyright © 2018 devchilango. All rights reserved.
//

#import "ResultadosController.h"

static NSString * celdaProducto = @"celdaProducto";

@interface ResultadosController ()

@end

@implementation ResultadosController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    UITapGestureRecognizer * tap = [[ UITapGestureRecognizer alloc ] initWithTarget:self action:@selector(volverBusqueda)];
    
    tap.numberOfTapsRequired = 1;
    
    [ _btnCerrar addGestureRecognizer:tap ];
    _btnCerrar.userInteractionEnabled = YES;
    
    
    
    
}

-(void)volverBusqueda{
    [ self dismissViewControllerAnimated:true completion:nil ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ProductoCell * cellVideo = [ tableView dequeueReusableCellWithIdentifier:celdaProducto forIndexPath:indexPath];
    NSString * strImagen = [[ _resultados objectAtIndex:indexPath.row ] objectForKey:@"imagen"];
    NSString * strTitulo = [[ _resultados objectAtIndex:indexPath.row ] objectForKey:@"titulo"];
    NSString * strPrecio = [[ _resultados objectAtIndex:indexPath.row ] objectForKey:@"precio"];
    
    cellVideo.titulo.text = strTitulo;
    cellVideo.precio.text = strPrecio;
    
    NSURL *url = [NSURL URLWithString: strImagen ];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    ProductoCell *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                    if (updateCell){
                        [ updateCell.imagen setImage:image ];
                    }
                });
            }
        }
    }];
    [task resume];
    
    
    return cellVideo;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [ _resultados count ];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
