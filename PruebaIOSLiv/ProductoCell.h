//
//  ProductoCell.h
//  PruebaIOSLiv
//
//  Created by lalo on 27/01/18.
//  Copyright © 2018 devchilango. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titulo;
@property (weak, nonatomic) IBOutlet UIImageView *imagen;
@property (weak, nonatomic) IBOutlet UILabel *precio;

@end
