//
//  BusquedaController.m
//  PruebaIOSLiv
//
//  Created by lalo on 27/01/18.
//  Copyright © 2018 devchilango. All rights reserved.
//

#import "BusquedaController.h"

static NSString * celdaAnterior = @"celdaAnterior";

@interface BusquedaController (){
    UIView *pantallaEspera;
    float altoPantalla;
    float anchoPantalla;
    NSMutableArray * resultados;
    NSMutableArray * arrBusquedasAnteriores;
}


@end

@implementation BusquedaController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults * prefs = [ NSUserDefaults standardUserDefaults ];
    arrBusquedasAnteriores = [ prefs objectForKey:keyPalabrasBusqueda ];
    if ( arrBusquedasAnteriores == nil ){
        arrBusquedasAnteriores = [ NSMutableArray new ];
    }else{
        arrBusquedasAnteriores = [ NSMutableArray arrayWithArray:arrBusquedasAnteriores ];
    }
    
    altoPantalla = [ UIScreen mainScreen ].bounds.size.height;
    anchoPantalla = [ UIScreen mainScreen ].bounds.size.width;
    
    
    _tableView.dataSource = self;
    _tableView.delegate = self;
    
    [ _tableView registerClass:[ UITableViewCell self ] forCellReuseIdentifier:celdaAnterior ];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *celda;
    
    celda = [ tableView dequeueReusableCellWithIdentifier:celdaAnterior forIndexPath:indexPath ];
    NSString * busqueda = [ arrBusquedasAnteriores objectAtIndex:indexPath.row ];
    celda.textLabel.text = busqueda;
    
    return celda;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [ arrBusquedasAnteriores count ];
}

- (IBAction)btnBuscarAction:(id)sender {
    
    NSUserDefaults * prefs = [ NSUserDefaults standardUserDefaults ];
    NSString * textoBusqueda = _txtBusqueda.text;
    
    //mostrar pantalla de expera
    [ self mostrarPantallaEspera ];
    
    //guardar el texto en user preferences
    [ arrBusquedasAnteriores addObject:textoBusqueda ];
    [ prefs setObject:arrBusquedasAnteriores forKey:keyPalabrasBusqueda ];
    [ prefs synchronize ];
    
    //llamar al servicio
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSURL *url = [NSURL URLWithString:[ NSString stringWithFormat:@"https://www.liverpool.com.mx/tienda?s=%@&d3106047a194921c01969dfdec083925=json", textoBusqueda ]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"GET";
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {

        NSError *jsonError;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
        
        resultados = [ NSMutableArray new ];
        
        if ( jsonError == nil ){
            
            NSArray * mainContent = [[[ json objectForKey:@"contents" ] objectAtIndex:0] objectForKey:@"mainContent"] ;
            NSDictionary * info = [[[ mainContent lastObject ] objectForKey:@"contents"] objectAtIndex:0];
            
            if ( [[ info objectForKey:@"totalNumRecs" ] intValue] > 0  ){
                
                NSArray * productos = [ info objectForKey:@"records" ];
                for ( NSDictionary * producto in productos ){
                    NSDictionary * attrProducto = [ producto objectForKey:@"attributes" ];
                    NSString * titulo = [[ attrProducto objectForKey:@"product.displayName" ] objectAtIndex:0];
                    NSString * precio = [[ attrProducto objectForKey:@"sku.list_Price" ] objectAtIndex:0];
                    NSString * imagen = [[ attrProducto objectForKey:@"sku.thumbnailImage" ] objectAtIndex:0];
                    
                    [ resultados addObject:@{ @"titulo":titulo, @"precio": precio, @"imagen": imagen }];
                }
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [ self performSegueWithIdentifier:@"segueProductos" sender:self ];
                    [ self ocultarPantallaEspera ];
                });
                
            }else{
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [ self ocultarPantallaEspera ];
                });
                //no se encontraron productos
            }
        }else{
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [ self ocultarPantallaEspera ];
            });
            //hubo un error
        }
        
    }];
    [dataTask resume];
    
    
    //generar un arreglo de productos y enviarlo al siguiente controlador
    
    //ocultar pantalla de busqueda
    
    //mandar a llamar al siguiente controlador
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ( [[ segue identifier ] isEqualToString:@"segueProductos"] ){
        ResultadosController * vc = [ segue destinationViewController ];
        vc.resultados = resultados;
    }
    
}

-(void)mostrarPantallaEspera{
    
    float anchoSpinner = 70;
    float altoSpinner = 70;
    
    pantallaEspera = [UIView new];
    pantallaEspera.frame = CGRectMake(0, 0, anchoPantalla, altoPantalla);
    [ pantallaEspera setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7] ];
    
    
    UIActivityIndicatorView  *spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    spinner.frame = CGRectMake((anchoPantalla/2) - (anchoSpinner/2), (altoPantalla/2) - (altoSpinner/2), anchoSpinner, altoSpinner );
    
    [pantallaEspera addSubview:spinner];
    [spinner startAnimating];
    
    [ self.view addSubview:pantallaEspera ];
    
}

-(void)ocultarPantallaEspera{
    [ pantallaEspera removeFromSuperview ];
}


@end
