//
//  BusquedaController.h
//  PruebaIOSLiv
//
//  Created by lalo on 27/01/18.
//  Copyright © 2018 devchilango. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constantes.h"
#import "ResultadosController.h"

@interface BusquedaController : UIViewController  < UITableViewDelegate, UITableViewDataSource >
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *txtBusqueda;
@property (weak, nonatomic) IBOutlet UIButton *btnBuscar;
- (IBAction)btnBuscarAction:(id)sender;




@end
